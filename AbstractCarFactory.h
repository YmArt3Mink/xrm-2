#pragma once

#include "AbstractBody.h"
#include "AbstractMotor.h"
#include "AbstractInterior.h"


class AbstractCarFactory 
{
public:
	virtual AbstractBody*
	CreateBody() const = 0;
	
	virtual AbstractMotor*
	CreateMotor() const = 0;
	
	virtual AbstractInterior*
	CreateInterior() const = 0;
};
