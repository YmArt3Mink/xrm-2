#include "Program.h"


int main(int argc, char* argv[])
{
	auto prog = new Program();
	prog->Run();

	return 0;
}
