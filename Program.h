#pragma once

#include "AbstractCarFactory.h"
#include "BMWFactory.h"
#include "LadaFactory.h"


class Program
{
private:
	static Program* _pInstance;

	AbstractCarFactory* _pFactory;

public:
	Program() {}
	~Program()
	{
		if (_pInstance != nullptr)
		{
			delete _pInstance;
		}

		if (_pFactory != nullptr)
		{
			delete _pFactory;
		}
	}

public:
	Program* GetInstance()
	{
		if (_pInstance == nullptr)
		{
			_pInstance = new Program();
		}

		return _pInstance;
	}

	void Run()
	{
		_pFactory 		= new BMWFactory();
		auto pBody 		= _pFactory->CreateBody();
		auto pMotor 	= _pFactory->CreateMotor();
		auto pInterior 	= _pFactory->CreateInterior();

		pBody->DoSomethingWithBody();
		pMotor->DoSomethingWithMotor();
		pInterior->DoSomethingWithInterior();

		_pFactory	= new LadaFactory();
		pBody 		= _pFactory->CreateBody();
		pMotor 		= _pFactory->CreateMotor();
		pInterior 	= _pFactory->CreateInterior();

		pBody->DoSomethingWithBody();
		pMotor->DoSomethingWithMotor();
		pInterior->DoSomethingWithInterior();
	}
};
