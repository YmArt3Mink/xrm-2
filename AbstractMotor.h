#pragma once


class AbstractMotor
{
public:
	virtual void
	DoSomethingWithMotor() const = 0;
};
