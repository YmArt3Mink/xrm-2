#pragma once

#include <iostream>

#include "AbstractMotor.h"


class LadaMotor : public AbstractMotor
{
public:
	LadaMotor() {}

public:
	virtual void
	DoSomethingWithMotor() const
	{
		std::cout << "LadaMotor is being used\n";
	}
};
