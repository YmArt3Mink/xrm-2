#pragma once

#include "AbstractCarFactory.h"
#include "LadaBody.h"
#include "LadaMotor.h"
#include "LadaInterior.h"


class LadaFactory : public AbstractCarFactory
{
public:
	LadaFactory() {}

public:
	virtual AbstractBody*
	CreateBody() const
	{
		return new LadaBody();
	}
	
	virtual AbstractMotor*
	CreateMotor() const
	{
		return new LadaMotor();
	}
	
	virtual AbstractInterior*
	CreateInterior() const
	{
		return new LadaInterior();
	}	
};
