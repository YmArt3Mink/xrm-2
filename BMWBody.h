#pragma once

#include <iostream>

#include "AbstractBody.h"


class BMWBody : public AbstractBody
{
public:
	BMWBody() {}
	virtual ~BMWBody() {}

public:
	virtual void
	DoSomethingWithBody() const
	{
		std::cout << "BMWBody is being used\n";
	}
};
