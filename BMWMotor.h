#pragma once

#include <iostream>

#include "AbstractMotor.h"


class BMWMotor : public AbstractMotor
{
public:
	BMWMotor() {}

public:
	virtual void
	DoSomethingWithMotor() const
	{
		std::cout << "BMWMotor is being used\n";
	}
};
