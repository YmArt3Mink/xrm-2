#pragma once

#include <iostream>

#include "AbstractBody.h"


class LadaBody : public AbstractBody
{
public:
	LadaBody() {}

public:
	virtual void
	DoSomethingWithBody() const
	{
		std::cout << "LadaBody is being used\n";
	}
};
