#pragma once

#include "AbstractCarFactory.h"
#include "BMWBody.h"
#include "BMWMotor.h"
#include "BMWInterior.h"


class BMWFactory : public AbstractCarFactory
{
public:
	BMWFactory() {}
	virtual ~BMWFactory() {}

public:
	virtual AbstractBody*
	CreateBody() const
	{
		return new BMWBody();
	}
	
	virtual AbstractMotor*
	CreateMotor() const
	{
		return new BMWMotor();
	}
	
	virtual AbstractInterior*
	CreateInterior() const
	{
		return new BMWInterior();
	}
};
