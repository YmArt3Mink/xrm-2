#pragma once


class AbstractBody
{
public:
	virtual void 
	DoSomethingWithBody() const = 0;
};
