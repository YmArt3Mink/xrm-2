#pragma once

#include <iostream>

#include "AbstractInterior.h"


class LadaInterior : public AbstractInterior
{
public:
	LadaInterior() {}

public:
	virtual void
	DoSomethingWithInterior() const
	{
		std::cout << "LadaInterior is being used\n";
	}
};
