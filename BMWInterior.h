#pragma once

#include <iostream>

#include "AbstractInterior.h"


class BMWInterior : public AbstractInterior
{
public:
	BMWInterior() {}

public:
	virtual void
	DoSomethingWithInterior() const
	{
		std::cout << "BMWInterior is being used\n";
	}
};
